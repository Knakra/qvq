# qvq (Quick Video Qonverter, yes with a Q)

Drag and Drop Video files onto the qvq.exe and it converts it to a smaller size with almost zero quality loss</br>

## Usage

Just download the [`qvq.exe`](https://gitlab.com/Knakra/qvq/-/raw/master/dist/qvq.exe) (or build it yourself) and place it somewhere like on your desktop.</br>
Now you can drag and drop the video files you want to convert onto the qvq.exe. </br>
The converted video will be besides the original with a _conv suffix.

## Build

Use build.cmd to make a build using pyinstaller. You need to have pyinstaller installed with `pip install pyinstaller`.</br>
