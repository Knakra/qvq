import sys, os
from pathlib import Path

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = Path(__file__).parent.absolute()

    return os.path.join(base_path, relative_path)

def main():
    if len(sys.argv) > 1:  # Checking if file path is provided
        for file_path in sys.argv[1:]:

            # Change the filename to contain _conv at the end before the extension
            file_path_split = file_path.split(".")
            file_path_new = ".".join(file_path_split[:-1]) + "_conv." + file_path_split[-1]

            ffmpeg_path = resource_path("ffmpeg\\ffmpeg.exe")
            os.system(ffmpeg_path + " -i \"" + file_path + "\" \"" + file_path_new + "\"")

        print("\n>> List of converted files:")
        for file_path in sys.argv[1:]:

            file_path_split = file_path.split(".")
            file_path_new = ".".join(file_path_split[:-1]) + "_conv." + file_path_split[-1]

            size_mb_old = os.path.getsize(file_path) / 1000000
            size_mb_old = round(size_mb_old, 2)

            size_mb_new = os.path.getsize(file_path_new) / 1000000
            size_mb_new = round(size_mb_new, 2)

            percentage_diff = (size_mb_new - size_mb_old) / size_mb_old * 100

            print("\n>> Original \"" + file_path + "\"")
            print(">> Converted \"" + file_path_new + "\"")
            print(">> From " + str(size_mb_old) + " MB" + " to " + str(size_mb_new) + " MB" + " (" + str(round(percentage_diff, 2)) + " %)")

        input("\n>> Press Enter to exit...")

if __name__ == "__main__":
    main()
